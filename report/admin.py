# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from models import DailyReport,Incident_report,Staff_report
from django.contrib import admin

# Register your models here.

class ToDisplay(admin.ModelAdmin):
    list_display=('date',)
    list_filter=('date',)

admin.site.register(Incident_report,ToDisplay)
admin.site.register(Staff_report,ToDisplay)

