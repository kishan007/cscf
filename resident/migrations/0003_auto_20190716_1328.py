# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-07-16 13:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('resident', '0002_auto_20190716_1132'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resident',
            name='contact_person',
            field=models.CharField(default='', max_length=100),
        ),
    ]
