# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# from django.contrib.auth import Group

from django.contrib import admin
from django.contrib.auth.models import Group,User
from models import Healthcondition,Details,SocialBackground,Progress_report,Welfare_case,Referer,From_Record,Police_report,FamilyBackground
from models import ChildsRecord,Personel_Record,Ambition_Record,Health_Record,Character_Record,Discipline_record,Visitor_record,Hospital_record
from models import Doc,Tour_record,Childs_fund,Favourites,Daily_report




# Register your models here.
class Photon(admin.ModelAdmin):
    list_display=('name','child_status_in_home','deleted')
    list_filter=('sex','religion','child_status_in_home')
    search_fields=('name','religion','roll_no')

class hd(admin.ModelAdmin):
    list_display=('roll_no',)
    # search_fields=('roll_no',)
    # list_filter=('sex','religion','language_can_speak','language_can_write')

class dc(admin.ModelAdmin):
    # month=
    list_display=('date','roll_no',)
    list_filter=('date',)
    search_fields=('date','name',)

# admin.site.unregister(Group)
# admin.site.unregister(User)
admin.site.site_header="Administration of SCF"
admin.site.site_title="Administration of SCF"
# admin.site.register(Child_Detail,Photon)
admin.site.register(Details,Photon)
admin.site.register(Healthcondition,hd)
admin.site.register(SocialBackground,hd)
admin.site.register(FamilyBackground,hd)
admin.site.register(Referer,hd)
admin.site.register(Welfare_case)
admin.site.register(From_Record,hd)
admin.site.register(Police_report,hd)
admin.site.register(Progress_report,hd)
admin.site.register(ChildsRecord,hd)
admin.site.register(Personel_Record,hd)
admin.site.register(Character_Record,hd)
admin.site.register(Ambition_Record,hd)
admin.site.register(Health_Record,hd)
admin.site.register(Favourites,hd)
admin.site.register(Doc,hd)
admin.site.register(Discipline_record,dc)
admin.site.register(Daily_report,dc)
admin.site.register(Visitor_record,dc)
admin.site.register(Hospital_record,dc)
admin.site.register(Tour_record,dc)
admin.site.register(Childs_fund,dc)


# admin.site.register(Document)
# admin.site.register(Student_Record)
# admin.site.register(Resident)
# admin.site.register(Contact_person)
