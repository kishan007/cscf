# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-07-16 16:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('user_id', models.AutoField(default=None, primary_key=True, serialize=False)),
                ('vehicle_no', models.CharField(default=None, max_length=100, unique=True)),
                ('driver_name', models.CharField(default=None, max_length=100)),
            ],
        ),
    ]
