# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Daily_status,Vehicle
from models import Maintenance,Road_tax_and_Insurance,Expenses,FuelExpenses


# Register your models here.
class ForVehicle(admin.ModelAdmin):
    list_display=('vehicle_no',)
    list_filter=('vehicle_no',)
    # search_fields=('vehicle_no',)

class Filter(admin.ModelAdmin):
    list_display=('vehicle_no','date','time_out','time_In')
    list_filter=('vehicle_no','date',)
    # search_fields=('vehicle_no','date',)

class Filters(admin.ModelAdmin):
    list_display=('vehicle_no','date',)
    list_filter=('vehicle_no','date',)
    # search_fields=('vehicle_no','date',)

admin.site.register(Vehicle,ForVehicle)
admin.site.register(Daily_status,Filter)
admin.site.register(Maintenance,Filters)
admin.site.register(Road_tax_and_Insurance,Filters)
admin.site.register(Expenses,Filters)
admin.site.register(FuelExpenses,Filters)
