# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import StudentCorner,StaffCorner
# Register your models here.
class ToDis(admin.ModelAdmin):
    list_display=('name','complaint_regarding','informed_to')
    search_fields=('name','complaint','informed_to')

admin.site.register(StudentCorner,ToDis)
admin.site.register(StaffCorner,ToDis)


