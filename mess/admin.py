# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from models import Menu,Budget
from django.contrib import admin


# Register your models here.
class ToDisplay(admin.ModelAdmin):
  list_display=('date','day')
  search_fields=('date','day')

class BudgetDis(admin.ModelAdmin):
      list_display=('date','time','quantity','unit','price')
      list_filter=('time','unit')
      search_fields=('date','time','unit')

admin.site.register(Menu,ToDisplay)
admin.site.register(Budget,BudgetDis)



